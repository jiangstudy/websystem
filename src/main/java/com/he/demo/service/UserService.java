package com.he.demo.service;

import com.he.demo.entities.Dish;
import com.he.demo.entities.TimeDoman;
import com.he.demo.entities.User;

import java.text.ParseException;
import java.util.List;

public interface UserService {

    List<User> getAllUsers();

    List<Dish> getDishes();

    void insertDish(Dish dish);

    void updateDish(Dish dish);

    void deleteDish(Dish dish);

    void insertUser(User user);

    void updateUser(User user);

    void deleteUser(User user);

    List<TimeDoman> getDate() throws ParseException;

    Object jdbcTest() throws ParseException;

    Object jdbcTest1();

    void jdbcTestForInsertWhenKeyContains();
}
