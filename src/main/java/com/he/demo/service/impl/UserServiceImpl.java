package com.he.demo.service.impl;
import com.he.demo.entities.Dish;
import com.he.demo.entities.TimeDoman;
import com.he.demo.entities.User;
import com.he.demo.entities.UserDao;
import com.he.demo.jobs.HelloJob;
import com.he.demo.mapper.UserMapper;
import com.he.demo.service.UserService;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<User> getAllUsers() {
        return userMapper.getUsers();
    }

    @Override
    public List<Dish> getDishes() {
        return userMapper.getDishes();
    }

    @Override
    public void insertDish(Dish dish) {
        userMapper.insertDish(dish);
    }

    @Override
    public List<TimeDoman> getDate() throws ParseException {
        TimeDoman timeDoman = new TimeDoman();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date time = simpleDateFormat.parse("2019-10-10");
        timeDoman.setTime(time);
        System.out.println(timeDoman);
        return userMapper.getDate(timeDoman);
    }

    @Override
    public void updateDish(Dish dish) {
        userMapper.updateDish(dish);
    }

    @Override
    public void deleteDish(Dish dish) {
        userMapper.deleteDish(dish);
    }

    @Override
    public void insertUser(User user) {
        userMapper.insertUser(user);
    }

    @Override
    public void updateUser(User user) {
        userMapper.updateUser(user);
    }

    @Override
    public void deleteUser(User user) {
        userMapper.deleteUser(user);
    }

    public Object jdbcTest() throws ParseException {
        String sql = "select * from time_test where Date(time) = ? ";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse("2019-10-10");
        List<Map<String, Object>> ans = jdbcTemplate.queryForList(sql, date);
        System.out.println("ans is " + ans);
        return ans;
    }

    public Object jdbcTest1() {
        String sql = "SELECT table_name FROM information_schema.TABLES WHERE table_name = ?";
        List<Map<String, Object>> ans = jdbcTemplate.queryForList(sql, "auth_user1");
        System.out.println(ans.size() == 0);
        System.out.println("the table name is " + ans);

        String tableName = "auth_user";
        String sql1 = "SELECT * from " + tableName;
        List<Map<String, Object>> detail = jdbcTemplate.queryForList(sql1);

        String count_sql = "SELECT COUNT(*) FROM " + tableName;

        int count = jdbcTemplate.queryForObject(count_sql, Integer.class);
        System.out.println("综数为 " + count);


        try {
            String name = jdbcTemplate.queryForObject("select name from auth_user where pwd = '1234'", String.class);
            System.out.println("name is " + name);
        }catch (EmptyResultDataAccessException e ) {
            System.out.println("异常 " + e);
        }


        List<String> nameList = jdbcTemplate.queryForList("select  name from auth_user where pwd = '1234'", String.class);
        System.out.println(nameList);


        List<UserDao> list;

        list = jdbcTemplate.query("select * from auth_user", (resultSet, i) -> {
            UserDao userDao = new UserDao();
            userDao.setId(resultSet.getLong("id"));
            userDao.setName(resultSet.getString("name"));
            userDao.setTime(resultSet.getTime("time"));
            userDao.setAccount(resultSet.getString("account"));
            userDao.setPwd(resultSet.getString("pwd"));
            return userDao;
        });
        System.out.println("list is " + list);
        return detail;
    }


    public void jdbcTestForInsertWhenKeyContains() {
        String sql = "insert into auth_user (id, name) values (1, 'john'), (2, 'zhangb')";
        jdbcTemplate.execute(sql);

        jdbcTemplate.execute(sql);

        String sql2 = "insert into auth_user (id, name) values (3, 'yaobin')";

        String sql3 = "insert into auth_user (id, name) values (1, 'jo')";

        jdbcTemplate.execute(sql2);

        System.out.println("sql - 2 success");

        jdbcTemplate.execute(sql3);
        System.out.println("all success");
    }

    public void testJob() throws SchedulerException {
        // 调度器 scheduler
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

        //任务示例, 与job绑定
        JobDetail jobDetail = JobBuilder.newJob(HelloJob.class)
                .withIdentity("jobKey1", "testGroup") // 参数1为任务的名称， 参数2为任务组的名称
                .build();
        // 触发器
        SimpleTrigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("triggerName1", "triggerGroup1")
                .startNow()
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(5).repeatForever())
                .build();

        //调度器关联任务和触发器
        scheduler.scheduleJob(jobDetail, trigger);

        System.out.println("准备启动");
        // 启动
        scheduler.start();
    }

}
