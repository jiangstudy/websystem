package com.he.demo.jobs;

import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class HelloJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        String dateString = new DateTime().toString("yyyy-MM-dd HH:mm:ss");
        System.out.println("the time is " + dateString);
    }
}
