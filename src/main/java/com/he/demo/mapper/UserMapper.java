package com.he.demo.mapper;

import com.he.demo.entities.Dish;
import com.he.demo.entities.TimeDoman;
import com.he.demo.entities.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface UserMapper {
    @Select("select * from user")
    List<User> getUsers();

    @Select("select * from dish")
    List<Dish> getDishes();

    @Insert("insert into dish(name,canteen,address,price,number) values(#{name},#{canteen},#{address},#{price},#{number})")
    void insertDish(Dish dish);

    @Update("update dish set name=#{name}, canteen=#{canteen}, address=#{address},price=#{price},number=#{number} where id=#{id}")
    void updateDish(Dish dish);

    @Delete("delete from dish where id=#{id}")
    void deleteDish(Dish dish);

    @Insert("insert into user(username, password,phone) values(#{username}, #{password}, #{phone})")
    void insertUser(User user);

    @Update("update user set username=#{username}, password=#{password}, phone=#{phone} where id={id}")
    void updateUser(User user);

    @Delete("delete from user where id=#{id}")
    void deleteUser(User user);

    @Select("select * from time_test where Date(time) = #{time}")
    List<TimeDoman> getDate(TimeDoman timeDoman);
}


