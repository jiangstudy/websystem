package com.he.demo.repository;

import com.he.demo.entities.User;
import com.he.demo.entities.UserDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface UserDaoRepository extends JpaRepository<UserDao, Object> {

    List<UserDao> findByNameAndAccountIn(String name, String[] pa);

    List<UserDao> findByNameAndTimeBetween(String name, Date date1, Date date2);

    Page<UserDao> findAll(Pageable pageable);

    @Query(nativeQuery = true,value = "select * from auth_user where name = ?1 limit 1")
    List<Object> myMethod(String pa);
}
