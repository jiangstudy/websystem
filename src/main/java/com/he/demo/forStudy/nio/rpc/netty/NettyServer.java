package com.he.demo.forStudy.nio.rpc.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

/**
 * 对nettyServer 进行初始化和启动
 */
public class NettyServer {

    public static void startServer(String hostName, int port) throws Exception {
        startServer0(hostName, port);
    }

    private static void startServer0(String hostName, int port) throws Exception {
        NioEventLoopGroup bossGroup = new NioEventLoopGroup(1);
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();

        ServerBootstrap serverBootstrap = new ServerBootstrap();

        serverBootstrap.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        ChannelPipeline pipeline = socketChannel.pipeline();
                        pipeline.addLast(new StringDecoder());
                        pipeline.addLast(new StringEncoder());
                        // 自定义业务处理器
                        pipeline.addLast(new RpcServerHandler());
                    }
                });

        ChannelFuture channelFuture = serverBootstrap.bind(hostName, port).sync();

        System.out.println("服务方启动成功");
        channelFuture.channel().closeFuture().sync();


        bossGroup.shutdownGracefully();
        workerGroup.shutdownGracefully();
    }
}
