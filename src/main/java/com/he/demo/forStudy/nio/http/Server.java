package com.he.demo.forStudy.nio.http;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import org.apache.logging.log4j.util.Chars;

public class Server {
    public static void main(String[] args) throws InterruptedException {

        // 创建 BossGroup  和 workerGroup   (都是循环)    默认给的线程数是  当前计算机核数的2倍
        NioEventLoopGroup bossGroup = new NioEventLoopGroup(); // 只处理 连接  请求
        NioEventLoopGroup workerGroup = new NioEventLoopGroup(); // 客户端的业务处理

        // 创建服务器端 启动对象
        ServerBootstrap bootstrap = new ServerBootstrap();
        // 配置启动参数
        bootstrap.group(bossGroup, workerGroup)   // 设置两个线程组
                .channel(NioServerSocketChannel.class)   // 使用NioServerSocketChannel作为服务器通道
                .childHandler(new ServerInitializer());// 给workerGroup的 EventLoop对应的管道设置处理器

        // 绑定端口并同步
        ChannelFuture channelFuture = bootstrap.bind(6668).sync();

        System.out.println("started");

        channelFuture.channel().closeFuture().sync();

//        bossGroup.shutdownGracefully();
//        workerGroup.shutdownGracefully();
    }

}


/**
 *  httpObject :  客户端和服务器端相互通讯的数据封装成 httpObject
 */
class HttpServerHandler extends SimpleChannelInboundHandler<HttpObject> {

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, HttpObject httpObject) throws Exception {
        System.out.println("get there");
        if (httpObject instanceof HttpRequest) {
            System.out.println("数据类型 : " + httpObject.getClass());
            System.out.println("客户端地址 " + channelHandlerContext.channel().remoteAddress());

            // 回复数据  (http协议）
            ByteBuf buf = Unpooled.copiedBuffer("hello, this is http server", CharsetUtil.UTF_8);
            // 构造 Http相应的回应， 即 httpResponse
            DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, buf);
            response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/plain");
            response.headers().set(HttpHeaderNames.CONTENT_LENGTH, buf.readableBytes());

            // 将response 发送
            channelHandlerContext.writeAndFlush(response);
        }
    }
}
