
# 使用netty自定义rpc


## 客户端（消费者）
```java
ClientBootStrap // 启动一个消费者

// netty
NettyClient
NettyClientHandler

// 创建代理对象
```

## 服务端
```java
HelloServiceImpl // 类
ServerBootStrap // 启动一个服务提供者

//netty
NettyServer
NettyServerHandler
```
