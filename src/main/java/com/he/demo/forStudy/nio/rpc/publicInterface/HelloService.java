package com.he.demo.forStudy.nio.rpc.publicInterface;

/**
 * 消费者和服务者公用的接口
 */
public interface HelloService {

    String hello(String msg);
}
