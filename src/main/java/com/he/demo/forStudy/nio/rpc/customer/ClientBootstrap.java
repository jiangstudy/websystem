package com.he.demo.forStudy.nio.rpc.customer;


import com.he.demo.forStudy.nio.rpc.netty.NettyClient;
import com.he.demo.forStudy.nio.rpc.publicInterface.HelloService;

/**
 *  消费方(客户端) 主启动类
 */
public class ClientBootstrap {

    /*
        消费方和提供方都遵循该规范，即以 "HelloService#hello#"开头， 才能远程调用提供方的方法
     */
    public static final String providerName = "HelloService#hello#";

    public static void main(String[] args) throws InterruptedException {
        NettyClient customer = new NettyClient();

        // 生成代理对象
        HelloService service = (HelloService) customer.getBean(HelloService.class, providerName);
        System.out.println("生成代理对象成功， 其hashcode 是： " + service.hashCode());
        Thread.sleep(10 * 1000);
        System.out.println("before 远程调用hello方法");
        String res = service.hello("你好， 我是test"); // 代码逻辑是在第一次调用方法的时候，才会去Init客户端
        System.out.println("after 远程调用方法， 返回结果是： " + res);
    }
}
