package com.he.demo.forStudy.nio.http;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;

public class ServerInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        // 向管道加入处理器

        // 得到管道
        ChannelPipeline pipeline = socketChannel.pipeline();

        // 添加netty中的handler
        pipeline.addLast("myHttpServerCodec", new HttpServerCodec());  // netty提供的编码解码器
        // 添加自己的handler
        pipeline.addLast("myHttpServerHandler",new HttpServerHandler());
    }
}
