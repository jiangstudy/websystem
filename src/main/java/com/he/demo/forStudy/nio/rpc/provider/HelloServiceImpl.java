package com.he.demo.forStudy.nio.rpc.provider;


import com.he.demo.forStudy.nio.rpc.publicInterface.HelloService;

public class HelloServiceImpl implements HelloService {

    /**
     * 消费方调用该方法的时候，就返回一个结果
     */
    @Override
    public String hello(String msg) {
        System.out.println("调用hello方法 once");
        System.out.println("收到客户端消息 " + msg);

        if (msg != null) return "服务方收到消息： " + msg;

        return "收到消息";

    }
}
