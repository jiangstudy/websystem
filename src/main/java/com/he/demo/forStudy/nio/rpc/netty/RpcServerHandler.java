package com.he.demo.forStudy.nio.rpc.netty;

import com.he.demo.forStudy.nio.rpc.provider.HelloServiceImpl;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class RpcServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        // 获取消费方发送的消息，并调用对应的方法
        System.out.println("msg is " + msg);
        // 自定义一个规范或者协议，消费方和提供方共同遵守
        System.out.println(msg);
        if (msg.toString().startsWith("HelloService#hello#")) { // 满足规范
            String result = new HelloServiceImpl().hello(msg.toString().substring(msg.toString().lastIndexOf("#") + 1));
            ctx.writeAndFlush(result);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println(cause.getMessage());
        ctx.close();
    }
}
