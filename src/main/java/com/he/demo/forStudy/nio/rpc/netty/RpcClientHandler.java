package com.he.demo.forStudy.nio.rpc.netty;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.concurrent.Callable;

public class RpcClientHandler extends ChannelInboundHandlerAdapter implements Callable {

    private ChannelHandlerContext ctx;

    private String result; // 返回的结果

    private String para; // 客户端调用方法时传入的参数


    /**
     *  与服务器连接，创建后即被调用
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("进入chanelActive方法");
        this.ctx = ctx;  //
    }

    /**
     * 收到数据后调用方法, 加了同步控制
     */
    @Override
    public synchronized void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("进入 chanelRead 方法");
        this.result = msg.toString();
        notify();  // 唤醒等待的线程
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println(cause.getMessage());
        ctx.close();
    }

    /**
     * 实现callAble中的方法
     * 被代理对象调用，发送数据给服务器， 发送结束后等待被唤醒
     */
    @Override
    public synchronized Object call() throws Exception {
        System.out.println("进入 call 方法" + this.para);
        this.ctx.writeAndFlush(this.para);  // 发送消息

        //等待获取到服务方的结果后唤醒，在channelRead中被唤醒
        wait();
        return this.result;
    }

    void setPara(String para) {
        System.out.println("设置para成功");
        this.para = para;
    }
}
