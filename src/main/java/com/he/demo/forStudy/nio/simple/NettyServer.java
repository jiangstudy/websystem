package com.he.demo.forStudy.nio.simple;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.CharsetUtil;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * this is a netty demo (server)
 */
public class NettyServer {
    public static void main(String[] args) throws InterruptedException {
//        String s = new DateTime(new Date()).toString("yyyy-MM-dd HH:mm:ss");
//        System.out.println(s);

        // 创建 BossGroup  和 workerGroup   (都是循环)    默认给的线程数是  当前计算机核数的2倍
        NioEventLoopGroup bossGroup = new NioEventLoopGroup(); // 只处理 连接  请求
        NioEventLoopGroup workerGroup = new NioEventLoopGroup(); // 客户端的业务处理

        // 创建服务器端 启动对象
        ServerBootstrap bootstrap = new ServerBootstrap();
        // 配置启动参数
        bootstrap.group(bossGroup, workerGroup)   // 设置两个线程组
            .channel(NioServerSocketChannel.class)   // 使用NioServerSocketChannel作为服务器通道
            .option(ChannelOption.SO_BACKLOG, 128)   // 设置线程队列得到连接个数
            .childOption(ChannelOption.SO_KEEPALIVE, true) // 设置保持活动连接状态
            .childHandler(new ChannelInitializer<SocketChannel>() {
                //  给对应的pipeline设置处理器
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    socketChannel.pipeline().addLast(new NettyServerHandler());
                }
            });// 给workerGroup的 EventLoop对应的管道设置处理器

        // 绑定端口并同步
        ChannelFuture channelFuture = bootstrap.bind(6668).sync();

        System.out.println("started");
        // 对关闭通道进行监听
        channelFuture.channel().closeFuture().sync();

        bossGroup.shutdownGracefully();
        workerGroup.shutdownGracefully();

    }
}

/**
 *  自定义 handler 并继承 netty的 adapter
 */
class NettyServerHandler extends ChannelInboundHandlerAdapter {

    /**
     *   读取数据
     *
     * @param ctx   上下文对象， 含有管道 pipeline, 也可以获取到 通道 channel， 以及地址
     * @param msg  客户端发送的数据
     * @throws Exception
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
         ByteBuf buf  = (ByteBuf) msg;
        System.out.println("from client : " + buf.toString(CharsetUtil.UTF_8));
        System.out.println("client's address is " + ctx.channel().remoteAddress());


        // 如果有个耗时的任务，可以提交到对应的 taskQueue中去异步执行：
//        Thread.sleep(10);
//        ctx.writeAndFlush(Unpooled.copiedBuffer("this is a long time task", CharsetUtil.UTF_8));

        // 提交到taskQueue
        ctx.channel().eventLoop().execute(() -> {
            // do long time task
        });


        // 还可以设置定时任务， 提交到 scheduleTaskQueue
        ctx.channel().eventLoop().schedule(() -> {
            // do a scheduled job
        }, 10, TimeUnit.SECONDS);
    }


    /**
     * 数据读取完毕
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        // 将数据写入到缓冲并刷新
        ctx.writeAndFlush(Unpooled.copiedBuffer("hello, this client", CharsetUtil.UTF_8));
    }

    /**
     * 处理异常
     * @param ctx
     * @param cause
     * @throws Exception
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }
}
