package com.he.demo.forStudy.nio.rpc.provider;


import com.he.demo.forStudy.nio.rpc.netty.NettyServer;

/**
 * 提供者(服务端) 主启动类
 */
public class ServerBootstrap {
    public static void main(String[] args) throws Exception{
        NettyServer.startServer("127.0.0.1", 7001); // 设置ip和端口并启动服务端
    }

}
