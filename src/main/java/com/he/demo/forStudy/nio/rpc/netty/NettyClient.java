package com.he.demo.forStudy.nio.rpc.netty;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.lang.reflect.Proxy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NettyClient {

    // 创建线程池
    private static ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    private static RpcClientHandler client;

    public Object getBean(final Class<?> serviceClass, final String providerName) {
        System.out.println("构造代理对象 ing ");
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                new Class<?>[]{serviceClass}, (proxy, method, args) -> {
                    // 此部分的代码，客户端每调用一次hello， 就会进入到该代码
                    if (client == null) {
                        System.out.println("第一次进入hello方法，开始初始化client");
                        initClient();
                        System.out.println("客户端初始化完成");
                    }
                    System.out.println("开始远程调用 " + providerName + args[0]);
                    // 设置给服务器发送的消息
                    client.setPara(providerName + args[0]);
                    return executor.submit(client).get();
                }
        );

    }

    private static void initClient() throws Exception {
        client = new RpcClientHandler();

        NioEventLoopGroup group = new NioEventLoopGroup();

        Bootstrap bootstrap = new Bootstrap();

        bootstrap.group(group)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.TCP_NODELAY, true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        ChannelPipeline pipeline = socketChannel.pipeline();
                        pipeline.addLast(new StringDecoder());
                        pipeline.addLast(new StringEncoder());
                        pipeline.addLast(client);
                    }
                });

        try {
            ChannelFuture channelFuture = bootstrap.connect("127.0.0.1", 7001).sync();
            System.out.println("消费方启动成功");
//            channelFuture.channel().closeFuture().sync();
        } finally {
//            group.shutdownGracefully();

        }

    }

}
