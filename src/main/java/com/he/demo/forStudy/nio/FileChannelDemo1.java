package com.he.demo.forStudy.nio;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FileChannelDemo1 {

    public static void main(String[] args) throws IOException {


        writeTest();
//        readTest();


    }

    public static void writeTest() throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("~/Desktop/b.txt");

        FileChannel channel = fileOutputStream.getChannel();

        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);

        String s = "this is a test of write";

        byteBuffer.put(s.getBytes());

        byteBuffer.flip();

        channel.write(byteBuffer);

        channel.close();
        fileOutputStream.close();


    }

    public static void readTest() throws IOException {
        final File file = new File("~/Desktop/b.txt");
        FileInputStream fileInputStream = new FileInputStream(file);

        FileChannel fileChannel = fileInputStream.getChannel();

        ByteBuffer byteBuffer = ByteBuffer.allocate((int) file.length());

        fileChannel.read(byteBuffer);

        // 将字节转化为字符串
        System.out.println(new String(byteBuffer.array()));

        fileChannel.close();
        fileInputStream.close();

    }

    /**
     * 使用byteBuffer， 复制文件， 一读一写
     * @throws IOException
     */
    public static void copyFile1() throws IOException {
        FileInputStream fileInputStream = new FileInputStream("./a.txt");

        FileOutputStream fileOutputStream = new FileOutputStream("./c.txt");

        FileChannel sourceChannel = fileInputStream.getChannel();
        FileChannel targetChannel = fileOutputStream.getChannel();

        ByteBuffer byteBuffer = ByteBuffer.allocate(10);

        while (true) { // 循环读取
            // 读
            byteBuffer.clear();  // 复位 ，重置标志位 ，不能省去
            int read = sourceChannel.read(byteBuffer);

            if (read != -1) {  // 已经读完
                break;
            }
            // 写
            byteBuffer.flip();
            targetChannel.write(byteBuffer);
        }

        fileInputStream.close();
        fileOutputStream.close();
    }

    /**
     * 复制文件2， 直接使用channel内容的复制
     * @throws IOException
     */
    public static void copyFile2() throws IOException {

        FileInputStream fileInputStream = new FileInputStream("./a.txt");

        FileOutputStream fileOutputStream = new FileOutputStream("./d.txt");

        FileChannel sourceChannel = fileInputStream.getChannel();
        FileChannel targetChannel = fileOutputStream.getChannel();

        targetChannel.transferFrom(sourceChannel, 0, sourceChannel.size());

        sourceChannel.close();
        targetChannel.close();
        fileInputStream.close();
        fileOutputStream.close();


    }
}
