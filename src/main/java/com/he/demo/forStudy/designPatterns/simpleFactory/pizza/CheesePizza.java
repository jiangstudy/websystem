package com.he.demo.forStudy.designPatterns.simpleFactory.pizza;

public class CheesePizza extends Pizza {
    @Override
    public void prepare() {
        System.out.println("给 cheesePizza 准备材料");
    }
}
