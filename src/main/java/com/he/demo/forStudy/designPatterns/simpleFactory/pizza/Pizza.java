package com.he.demo.forStudy.designPatterns.simpleFactory.pizza;

/**
 * 披萨的抽象类
 */
public abstract class Pizza {

    protected String name; // 名字

    public abstract void prepare(); // 不同的种类，准备材料不一样，使用抽象方法

    // 烘焙，方式大同小异
    public void bake() {
        System.out.println(name + " baking;");
    }

    // 切割
    public void cut() {
        System.out.println(name + " cutting");
    }

    // 打包
    public void box() {
        System.out.println(name + " boxing");
    }

    public void setName(String name) {
        this.name = name;
    }
}
