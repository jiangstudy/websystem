package com.he.demo.forStudy.designPatterns.simpleFactory.order;

import com.he.demo.forStudy.designPatterns.simpleFactory.pizza.CheesePizza;
import com.he.demo.forStudy.designPatterns.simpleFactory.pizza.GreekPizza;
import com.he.demo.forStudy.designPatterns.simpleFactory.pizza.Pizza;

import java.util.Scanner;

public class OrderPizza {

    public OrderPizza() {
        Pizza pizza = null;

        String orderType; // 订购披萨的类型

        do {
            orderType = getType();
            if ("greek".equals(orderType)) {
                pizza = new GreekPizza();
                pizza.setName("greekPizza");
            } else if ("cheese".equals(orderType)) {
                pizza = new CheesePizza();
                pizza.setName("cheesePizza");
            } else {
                break;
            }

            pizza.prepare();
            pizza.bake();
            pizza.cut();
            pizza.box();
        } while (true);
    }

    private String getType() {
        Scanner scanner = new Scanner(System.in);
        // ....
        while (scanner.hasNextLine()) {
            return scanner.next();
        }

        return null;
    }
}
