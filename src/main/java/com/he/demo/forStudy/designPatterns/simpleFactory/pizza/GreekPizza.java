package com.he.demo.forStudy.designPatterns.simpleFactory.pizza;

public class GreekPizza extends Pizza {

    @Override
    public void prepare() {
        System.out.println("给 greekPizza 准备材料");
    }
}
