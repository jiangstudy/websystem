package com.he.demo.forStudy.designPatterns;

/**
 * 单例模式
 *
 */
public class SingleTon {

    public static void main(String[] args) {
        Singleton1 instance = Singleton1.getInstance();
        Singleton1 instance1 = Singleton1.getInstance();
        System.out.println(instance == instance1);

        System.out.println("----");
        Singleton8 instance2 = Singleton8.INSTANCE;
        Singleton8 instance3 = Singleton8.INSTANCE;
        System.out.println(instance2 == instance3);

        System.out.println("---");

        Runtime runtime = Runtime.getRuntime();  // java.lang.Runtime就是单例模式，采用的是方式1， 一定会使用到
        Runtime runtime1 = Runtime.getRuntime();
        System.out.println(runtime == runtime1);
    }
}

/**
 * 方式1， 构造器私有化, 防止外部new
 *
 *  优点：类装载的时候就实例化，避免线程同步问题
 *  缺点： 不能懒加载
 */
class Singleton1 {

    // 构造器私有化
    private Singleton1() {

    }
    // 类装载的时候，实例化
    private final static Singleton1 instance = new Singleton1();

    // 对外提供共有的静态方法，返回实例
    public static Singleton1 getInstance() {
        return instance;
    }
}

/**
 *  方式2，也是饿汉式， 使用静态代码块，和方式1很类似，也是在类装载的时候实例化
 */
class Singleton2 {

    private Singleton2() {

    }

    private static Singleton2 instance;

    static {
        instance = new Singleton2();
    }

    public static Singleton2 getInstance() {
        return instance;
    }
}

/**
 * 方式3 懒汉式  第一次使用的时候构造
 *
 * 线程不安全，如果有一个线程进入到了 if(instance == null) 中， 还没有往下执行，另一个线程也通过了这个判断语句，则会产生多个实例
 */
class Singleton3 {
    private static Singleton3 instance;

    private Singleton3() {

    }

    // 提供静态的公有方法
    public static Singleton3 getInstance() {
        if (instance == null) {
            instance = new Singleton3();
        }
        return instance;
    }
}

/**
 * 方式4， 懒汉式， 判断语句加入锁
 * 解决线程安全问题，但是效率低
 */
class Singleton4 {
    private static Singleton4 instance;

    private Singleton4() {

    }

    public static synchronized Singleton4 getInstance() {
        if (instance == null) {
            instance = new Singleton4();
        }
        return instance;
    }
}

/**
 * 双重检查，能解决线程安全问题，也能解决效率问题， 推荐使用的一种方式
 */
class Singleton5 {
    private static volatile Singleton5 instance;  // 共享变量，一旦改变，会刷新到共享变量中， volatile必须加上
    private Singleton5() {

    }

    public static Singleton5 getInstance() {
        if (instance == null) {
            synchronized (Singleton5.class) {
                if (instance == null) {
                    instance = new Singleton5();
                }
            }
        }
        return instance;
    }
}

/**
 * 使用静态内部类， 也是推荐使用的方式
 * 是线程安全的，
 *
 *
 */
class Singleton6 {
    private Singleton6() {

    }

    //采用类装载的机制保证单例，类装载是线程安全的，调用getInstance时，才会装载SingleInstance类
    private static class SingletonInstance {
        private static final Singleton6 INSTANCE = new Singleton6();
        //类的静态属性只会在第一次加载类的时候初始化，所以jvm保证了线程的安全性，在类进行初始化的时候，别的线程无法进入
    }

    public static Singleton6 getInstance() {
        return SingletonInstance.INSTANCE;
    }
}

/**
 * 采用枚举类， 也是推荐使用的一种方式
 * 能避免多线程同步问题，还能防止反序列化重新创建新的对象
 */
enum Singleton8 {
    INSTANCE; // 属性
}
