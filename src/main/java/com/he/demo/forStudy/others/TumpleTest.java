package com.he.demo.forStudy.others;

import java.util.Arrays;

public class TumpleTest {
    public static void main(String[] args) {
        TupleOfTwo<String, Integer> ans = new TupleOfTwo<>("12138", 12139);
        System.out.println(ans);

        System.out.println("---");
        int[][] nums = {{1, 2, 6, 2}};
        String[] strings = {"abc", "abcd", "fhd", "ihhs"};
        Arrays.sort(nums, (s1, s2) -> (s2[0] - s1[0]));
        System.out.println(Arrays.toString(strings));

        System.out.println("--");
        String str = "hello//world/../ ";
        String[] strList = str.split("/");
        System.out.println(strList.length);
        System.out.println(Arrays.toString(strList));



    }
}

class TupleOfTwo<String, Integer> {
    private String first;
    private Integer second;

    public TupleOfTwo(String first, Integer second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public java.lang.String toString() {
        return "TupleOfTwo{" +
                "first=" + first +
                ", second=" + second +
                '}';
    }
}
