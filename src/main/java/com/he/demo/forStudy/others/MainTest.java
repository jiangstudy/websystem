package com.he.demo.forStudy.others;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class MainTest {
    public static void main(String[] args) {
        Class<MainTest> mainTestClass = MainTest.class;
        Method[] declaredMethods = mainTestClass.getDeclaredMethods();
        Stream.of(declaredMethods).forEach(method -> {
            MyAutoWired annotation = method.getAnnotation(MyAutoWired.class);
            if (annotation != null) {
                test();
            }

        });

        System.out.println("----");
        int[] nums = {2, 3, 6, 7};
        int target = 20;

        MainTest mainTest = new MainTest();
        List<List<Integer>> ans = mainTest.method(nums, target);
        System.out.println(ans);

        System.out.println(ans.size());
        System.out.println("total is " + mainTest.getNum(nums, 0, target));



    }

    @MyAutoWired
    public static void test() {
        String[] array = {"111", "222", "333"};

        Stream.of(array).forEach(System.out::print);

    }

    public  List<List<Integer>> method(int[] nums, int target) {

        Deque<Integer> path = new LinkedList<>();
        List<List<Integer>> ans = new ArrayList<>();
        dfs(nums, 0, nums.length, target, ans, path);
        return ans;
    }

    /**
     * 求详细的组合， 需要使用回溯，构建一个path，满足条件时加入到结果集中
     */
    public void dfs(int[] nums, int begin, int length, int target, List<List<Integer>> ans, Deque<Integer> path) {
        if (target == 0) {
            ans.add(new ArrayList<>(path));
            return;
        }
        if (target < 0) return;

        for (int i = begin; i < length; i++) {
            path.addLast(nums[i]);

//            System.out.println("递归之前 " + path + " 剩余 " + (target - nums[i]));
            dfs(nums, i, length, target - nums[i], ans, path);

            path.removeLast();
//            System.out.println("------递归之后 " + path);
        }
    }


    /**
     * 只求总的个数，简单递归
     */
    public int getNum(int[] nums, int begin, int target) {
        int ans = 0;
        for (int i = begin; i < nums.length; i++) {
            if (nums[i] == target) {
                ans += 1;
                break;
            }
            if (nums[i] > target) break;
            ans += getNum(nums, i, target - nums[i]);
        }
        return ans;
    }

}
