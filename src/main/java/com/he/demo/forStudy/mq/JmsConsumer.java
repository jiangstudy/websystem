package com.he.demo.forStudy.mq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 *  消费方
 */
public class JmsConsumer {


    private static final String ACTIVEMQ_URL = "tcp://39.102.36.12:61616";

    private static final String QUEUE_NAME = "queue01";

    public static void main(String[] args) throws Exception {

        // 1. 创建 连接 工厂
        ActiveMQConnectionFactory mqConnectionFactory = new ActiveMQConnectionFactory(ACTIVEMQ_URL);

        // 2. 通过工厂，获得连接
        Connection connection = mqConnectionFactory.createConnection();
        connection.start();

        // 3. 创建会话, 参数1，为事务， 参数2 为 签收
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        // 4. 创建目的地 (是 队列queue 还是 主题topic)
        Queue destination = session.createQueue(QUEUE_NAME);  // Queue 不是 list类的接口，而是 jms下的接口

        // 5. 创建消息的消费者
        MessageConsumer consumer = session.createConsumer(destination);

        // 消息 消费

        /*
            receive 方式，同步阻塞， 可以设置 timeout,, 会一直阻塞直到获取到消息
        for (;;){
            TextMessage receiveMessage = (TextMessage) consumer.receive();
            if (receiveMessage != null)  System.out.println("收到消息 : " + receiveMessage.getText());
            else break;
        }

         */

        /*
            方式2， 采用监听器
         */
        consumer.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                if (message != null && message instanceof TextMessage) {
                    TextMessage textMessage = (TextMessage) message;
                    try {
                        System.out.println("*** 监听到消息： " + textMessage.getText());
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        System.in.read();  // 保证控制台持续打开

        // 关闭资源
        session.close();
        connection.close();
    }
}
