package com.he.demo.forStudy.mq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * 消息生产者
 * queue是 1对 1
 * topic 是 1对多
 */
public class JmsProduce {

    private static final String ACTIVEMQ_URL = "tcp://39.102.36.12:61616";

    private static final String QUEUE_NAME = "queue01";

    public static void main(String[] args) throws Exception {

        // 1. 创建 连接 工厂
        ActiveMQConnectionFactory mqConnectionFactory = new ActiveMQConnectionFactory(ACTIVEMQ_URL);

        // 2. 通过工厂，获得连接
        Connection connection = mqConnectionFactory.createConnection();
        connection.start();

        // 3. 创建会话, 参数1，为事务， 参数2 为 签收
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        // 4. 创建目的地 (是 队列queue 还是 主题topic)
        Queue destination = session.createQueue(QUEUE_NAME);  // Queue 不是 list类的接口，而是 jms下的接口
        /*
        如果是 topic， 则 使用该语句
           Topic destination = session.createTopic(QUEUE_NAME);

         */
        // 5. 创建消息的生产者
        MessageProducer producer = session.createProducer(destination);

        // 6. 发送消息到 mq的队列里面
        for (int i = 0; i < 3; i++) {
            // 创建消息
            TextMessage textMessage = session.createTextMessage("msg ----  " + i + " 条消息");
            producer.send(textMessage);
        }

        // 关闭资源
        producer.close();
        session.close();
        connection.close();
        System.out.println("*** 消息发布到mq完成");
    }
}
