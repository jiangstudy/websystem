package com.he.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.he.demo.entities.LogRecords;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MainTest {


    public static void main(String[] args) {

        Map<String,Object> logMap=new HashMap<>();
        logMap.put("userName","李放");//账号
        logMap.put("realName","李放");//姓名
        logMap.put("department","技术部");//部
        logMap.put("module", "团队管理");
        logMap.put("type","解绑角色");
        logMap.put("details","将"+"赵雪梅"+"解绑'"+"管理员"+"'角色");

        LogRecords logRecords=new LogRecords();
        logRecords.setType(io.touty.gh.management.enums.LogType.AdminOperationLog);
        logRecords.setOperationTime(new Date());
        logRecords.setData(logMap);


        System.out.println(JSONObject.toJSONString(logRecords));
        System.out.println("hello");

        Class<MainTest> mainTestClass = MainTest.class;

        MainTest mainTest = new MainTest();
        Integer i = null;
        mainTest.method(i);
        Object o = null;

    }

    public static void method(Object o) {
        System.out.println("objetc");
    }
    public static  void method(Integer a) {
        System.out.println("int");
    }
}
