package com.he.demo.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.he.demo.entities.*;
import com.he.demo.mapper.UploadDAO;
import com.he.demo.repository.UserDaoRepository;
import com.he.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.DataFormatException;


@RestController
@CrossOrigin
public class TestController {
    @Autowired
    UserService userService;

    @Autowired
    private UserDaoRepository userDaoRepository;



    @RequestMapping("/boot/test")
    public String test() {
        return "this is a test";
    }

    @RequestMapping("/boot/user")
    public User getUser() {
        User user = new User();
        user.setId(12);
        user.setUsername("heshuai");

        return user;
    }

    @RequestMapping("/boot/get")
    public Object getUsers() {
        ReturnType<User> returnType = new ReturnType<>();
        returnType.setCode(200);
        returnType.setData(userService.getAllUsers());
        System.out.println("查询所有的学生once");
        System.out.println("---------------");
        return returnType;
    }

    @RequestMapping("/boot/getDishes")
    public Object getDished() {
        ReturnType<Dish> returnType = new ReturnType<>();
        returnType.setCode(200);
        returnType.setData(userService.getDishes());
        System.out.println("查询所有的dish once");
        System.out.println("-----------");
        return returnType;
    }

    @GetMapping("/boot/insertDish")
    public Object insertDish(@RequestParam("id") String id, @RequestParam("name") String name,
                            @RequestParam("canteen") String canteen, @RequestParam("address") String address,
                            @RequestParam("price") String price, @RequestParam("number") String number) {


        if(id == null || "null".equals(id)) {
            ReturnType returnType = new ReturnType();
            returnType.setCode(404);
            return returnType;
        }
        Dish dish = new Dish();
        dish.setId(Integer.parseInt(id));
        dish.setName(name);
        dish.setCanteen(canteen);
        dish.setAddress(address);
        dish.setPrice(price);
        dish.setNumber(number);
        userService.insertDish(dish);
        System.out.println("插入一个新的dish once");
        System.out.println("------------");
        ReturnType<Object> returnType = new ReturnType();
        returnType.setCode(200);
        return returnType;
    }

    @GetMapping("/boot/updateDish")
        public Object updateDish(@RequestParam("id") String id, @RequestParam("name") String name,
                @RequestParam("canteen") String canteen, @RequestParam("address") String address,
                @RequestParam("price") String price, @RequestParam("number") String number) {
        if(id == null || "null".equals(id)) {
            ReturnType returnType = new ReturnType();
            returnType.setCode(404);
            return returnType;
        }
            Dish dish = new Dish();
            dish.setId(Integer.parseInt(id));
            dish.setName(name);
            dish.setCanteen(canteen);
            dish.setAddress(address);
            dish.setPrice(price);
            dish.setNumber(number);
            userService.updateDish(dish);
        System.out.println("更新dish once");
        System.out.println("---------");
            ReturnType<Object> returnType = new ReturnType();
            returnType.setCode(200);
            return returnType;
        }


    @GetMapping("/boot/deleteDish")
    public Object deleteDish(@RequestParam("id") String id) {
        if(id == null || "null".equals(id)) {
            ReturnType returnType = new ReturnType();
            returnType.setCode(404);
            return returnType;
        }
        System.out.println("准备删除id : " + id);
        Dish dish = new Dish();
        dish.setId(Integer.parseInt(id));
        userService.deleteDish(dish);
        System.out.println("删除dish once");
        System.out.println("----------");
        ReturnType returnType = new ReturnType();
        returnType.setCode(200);
        return returnType;

    }

    @GetMapping("/boot/insertUser")
    public Object insertDish(@RequestParam("id") String id, @RequestParam("username") String username,
                             @RequestParam("password") String password, @RequestParam("phone") String phone)
    {


        if(id == null || "null".equals(id)) {
            ReturnType returnType = new ReturnType();
            returnType.setCode(404);
            return returnType;
        }
        User user = new User();
        user.setId(Integer.parseInt(id));
        user.setUsername(username);
        user.setPassword(password);
        user.setPhone(phone);
        userService.insertUser(user);
        System.out.println("插入一个新的用户 once");
        System.out.println("------------");
        ReturnType<Object> returnType = new ReturnType();
        returnType.setCode(200);
        return returnType;
    }

    @GetMapping("/boot/deleteUser")
    public Object deleteUser(@RequestParam("id") String id) {
        if(id == null || "null".equals(id)) {
            ReturnType returnType = new ReturnType();
            returnType.setCode(404);
            return returnType;
        }
        System.out.println("删除id : " + id);
        User user = new User();
        user.setId(Integer.parseInt(id));
        userService.deleteUser(user);
        ReturnType returnType = new ReturnType();
        returnType.setCode(200);
        return returnType;

    }

    @GetMapping("/boot/updateUser")
    public Object updateUser(@RequestParam("id") String id, @RequestParam("username") String username,
                             @RequestParam("password") String password, @RequestParam("phone") String phone)
    {


        if(id == null || "null".equals(id)) {
            ReturnType returnType = new ReturnType();
            returnType.setCode(404);
            return returnType;
        }
        User user = new User();
        user.setId(Integer.parseInt(id));
        user.setUsername(username);
        user.setPassword(password);
        user.setPhone(phone);
        userService.updateUser(user);

        ReturnType<Object> returnType = new ReturnType();
        returnType.setCode(200);
        return returnType;
    }

    @GetMapping("/boot/tableName")
    public Object timeTest() throws ParseException {
//        List<TimeDoman> names = userService.getDate();
        Object o = userService.jdbcTest1();
        System.out.println(o);

        return o;
    }


//    @PostMapping("/boot/insertDishByPost")
//    public Object insertDish(@RequestBody Dish dish) {
//        System.out.println("the id  of post is " + dish.getId());
//        ReturnType<Object> returnType = new ReturnType<>();
//        returnType.setCode(200);
//        return returnType;
//    }

    @GetMapping("/boot/jpaTest")
    public Object jpaTest() throws ParseException {

        List<UserDao> list = null;
        UserDao userDao = new UserDao();
        userDao.setName("john123");
        userDao.setAccount("2019111");
        userDao.setId(123l);
        try {
//            userDaoRepository.save(userDao);

            List<String> pa = new ArrayList<>();

            list = userDaoRepository.findByNameAndAccountIn("john", new String[]{"2019111", "2019112"});
            System.out.println(list);

        }catch (Exception e) {
            System.out.println(e);
        }

        return list;
    }

    @GetMapping("/boot/jpaTime")
    public Object jpaTime() {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = null;
        Date date2 = null;
        List<UserDao> list = null;
        try {
            date1 = simpleDateFormat.parse("2020-06-01");
            date2 = simpleDateFormat.parse("2020-07-08");
        } catch (Exception e) {
            System.out.println("日期创建错误 " + e);
        }


        try {


            list = userDaoRepository.findByNameAndTimeBetween("john", date1, date2);
            System.out.println(list);

        }catch (Exception e) {
            System.out.println(e);
        }

        return list;
    }

    @GetMapping("/boot/jpaSave")
    public Object jpaSave() throws ParseException {

        UserDao userDao = new UserDao();
        userDao.setName("john");
        userDao.setAccount("2019111");
        userDao.setId(123l);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = simpleDateFormat.parse("2020-07-01 02:12:34");
            date2 = simpleDateFormat.parse("2020-07-05 14:12:32");
        } catch (Exception e) {
            System.out.println("日期创建错误 " + e);
        }

        userDao.setTime(date1);
        UserDao userDao1 = new UserDao();
        userDao1.setName("john");
        userDao1.setAccount("2019112");
        userDao1.setId(125l);
        userDao1.setTime(date2);

        UserDao userDao2 = new UserDao();
        userDao2.setName("zhang");
        userDao2.setAccount("2019113");
        userDao2.setId(1l);

        UserDao userDao3 = new UserDao();
        userDao3.setName("heshuai");
        userDao3.setAccount("2019114");
        userDao3.setId(2l);
        try {
            userDaoRepository.save(userDao);
            userDaoRepository.save(userDao1);
            userDaoRepository.save(userDao2);
            userDaoRepository.save(userDao3);


        }catch (Exception e) {
            System.out.println(e);
        }

        return "success";
    }

    @GetMapping("/boot/jpaPage")
    public Object jpaPage() throws ParseException {

        Page<UserDao> userDaoPage = null;

        try {

            userDaoPage = userDaoRepository.findAll(new PageRequest(0,3));
            System.out.println(userDaoPage);

            System.out.println("--------");
            for (UserDao userDao : userDaoPage) {
                System.out.println(userDao);
                userDao.setName("new name");
            }


        }catch (Exception e) {
            System.out.println(e);
        }

        return userDaoPage;
    }

    @GetMapping("/boot/test2")
    public Object test2() throws ParseException {


        Object oneName = userDaoRepository.myMethod("zhang");
        System.out.println("the name is " + oneName);
        return  oneName;
    }

    @GetMapping("/boot/test3")
    public Object test3() throws ParseException {

        userService.jdbcTestForInsertWhenKeyContains();
        return "1";
    }


    @Autowired
    private UploadDAO uploadDAO;

    @PostMapping("/upload")
    @ResponseBody
    public Object testExcel(MultipartFile multipartFile, String name, @RequestHeader("x_project_id") String id)  {
        UserDao user = new UserDao();
        user.setName("john");
        user.setAccount("heshuai");
        System.out.println("进入方法" + name + " " + id);
        System.out.println(multipartFile);
        try {
            EasyExcel.read(multipartFile.getInputStream(), UploadData.class, new UploadDataListener(uploadDAO, user)).sheet().doRead();
        } catch (IOException e) {

        }  catch (Exception e) {
            if (e instanceof MyException) {
                System.out.println("异常了，不要继续下去了");
                System.out.println(e);
                return "fail";
            }
            System.out.println("其他异常");
            return "unsucess";
        }
        return "success";

    }


    @GetMapping("/download")
    public void download(HttpServletResponse response) throws IOException {
        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = URLEncoder.encode("测试", "UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), DownloadData.class).sheet("模板").doWrite(data());
    }

    private List<DownloadData> data() {
        List<DownloadData> list = new ArrayList<DownloadData>();
        for (int i = 0; i < 10; i++) {
            DownloadData data = new DownloadData();
            data.setString("字符串" + 0);
            data.setDate(new Date());
            data.setDoubleData(0.56);
            list.add(data);
        }
        return list;
    }

    @PostMapping("/test123")
    public Object test123(@RequestBody TestBodyDto body) {
        System.out.println("---");
        System.out.println(body);
        System.out.println("---");
        System.out.println("name is " + body.getName());
        System.out.println(".....");
        System.out.println(body.getList().size() + " " + body.getList().get(0));

        return "success";
    }

    @PostMapping("/listTest")
    public Object listTest(@RequestBody Map<String, List<String>> body) {

        System.out.println(body);
        return body.get("idList");
    }




    private List data1(String type) {
        List<DownloadData> list = new ArrayList<DownloadData>();
        for (int i = 0; i < 10; i++) {
            DownloadData data = new DownloadData();
            data.setString("字符串" + 0);
            data.setDate(new Date());
            data.setDoubleData(0.56);
            list.add(data);
        }
        return list;
    }

    public static void main(String[] args)  {


        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT into testWaitPeople values ");
        String base = "13262";
        long number = 531830;

        for (int i = 0; i < 200000; i++) {
            String userId = base + (number ++);
            stringBuilder.append("( '").append(userId).append("' , 1.00, 20200805, 'unit', 'cust', 'info'), \n");
        }

        String s= stringBuilder.toString();


        FileWriter fw ;
        File f = new File("c.txt");

        try {
            if(!f.exists()){
                f.createNewFile();
            }
            fw = new FileWriter(f);
            BufferedWriter out = new BufferedWriter(fw);
            out.write(s, 0, s.length()-1);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("end");


    }



}
