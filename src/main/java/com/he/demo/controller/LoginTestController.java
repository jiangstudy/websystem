package com.he.demo.controller;


import com.he.demo.entities.Child;
import com.he.demo.entities.XmlResponce;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class LoginTestController {

    @GetMapping("/loginTest/test1")
    public Object test1(HttpServletRequest request, HttpServletResponse response, @RequestParam(name = "appid", required = false) String appId) throws IOException {
        System.out.println("----------");
        StringBuffer urlBuff = request.getRequestURL();
        System.out.println("url is :" + urlBuff);

        String url = urlBuff.toString();
        String service;
        if (url.contains("?")) {
            service = url +  "&appid=12138";
        } else if (url.endsWith("/")) {
            service = url.substring(url.length() - 1) + "?&appid=12138";
        } else {
            service = url + "?&appid=12138";
        }

        if (!url.contains("appid")) {
            response.sendRedirect("http://localhost:8080/forTest/loginTest/test2?service=" + service);
        }
        System.out.println("appid is " + appId);

        return "test1";
    }

    @RequestMapping(value = "/loginTest/test2")
    public Object test2(HttpServletRequest request, HttpServletResponse response, @RequestParam(name = "appid") String appId) throws IOException {
        System.out.println("----------");
        System.out.println("appid is " + appId);
        String url = request.getRequestURL().toString();
        System.out.println("url is " + url);
        String queryString = request.getQueryString();
        String toUrl = request.getQueryString();
        System.out.println("service is " + toUrl);

        System.out.println("url2 is :" + url);

        response.sendRedirect(toUrl);
        return "test2";
    }


    @PostMapping(value = "/xml")
    public Object test4(HttpServletRequest request, HttpServletResponse response) throws IOException {

        System.out.println("准备返回xml");
        XmlResponce xmlResponce = new XmlResponce();
        Child child = new Child();
        child.setName("john");
        child.setIsWrong("trueeee");
        xmlResponce.setChild(child);
        return xmlResponce;
    }


    @GetMapping("/loginTest/test3")
    public Object test3(HttpServletRequest request, HttpServletResponse response, @RequestParam(name = "service") String service) throws IOException {
        System.out.println("----------");
        String url = request.getRequestURL().toString();
        System.out.println("url is " + url);

//        String queryString = request.getQueryString();
//        String toUrl = request.getQueryString();
        System.out.println("service is " + service);

        System.out.println("url2 is :" + url);

        response.sendRedirect( service + "?ticket=123456");
        return "test2";
    }


    @GetMapping(value = "/xml")
    public Object test5(HttpServletRequest request, HttpServletResponse response) throws IOException {

        System.out.println("准备返回xml");
        XmlResponce xmlResponce = new XmlResponce();
        Child child = new Child();
        child.setName("john");
        child.setIsWrong("trueeee");
        xmlResponce.setChild(child);
        return xmlResponce;
    }




}
