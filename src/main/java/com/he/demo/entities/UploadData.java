package com.he.demo.entities;

import com.alibaba.excel.annotation.ExcelProperty;

public class UploadData {

    @ExcelProperty("name")
    private String name;

    @ExcelProperty("desc")
    private String desc;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    @Override
    public String toString() {
        return "UploadData{" +
                "name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
