package com.he.demo.entities;

import lombok.Data;

@Data
public class Dish {
    private int id;
    private String name;
    private String canteen;
    private String address;
    private String price;
    private String number;
}
