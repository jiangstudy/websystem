package com.he.demo.entities;

import lombok.Data;

@Data
public class Admin {
    private int id;
    private String username;
    private String password;
    private String phone;
}
