package com.he.demo.entities;

import lombok.Data;

import java.util.List;

@Data
public class ReturnType<T> {
    private int code;
    private List<T> data;
}
