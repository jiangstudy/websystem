package com.he.demo.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "TEST")
public class XmlResponce {

private Child child;


    public Child getChild() {
        return child;
    }

    @XmlElement(name = "RES")
    public void setChild(Child child) {
        this.child = child;
    }


}
