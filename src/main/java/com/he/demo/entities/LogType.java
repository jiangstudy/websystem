package io.touty.gh.management.enums;

public enum LogType {
    //登录日志
    LoginLog,
    //管理员操作日志
    AdminOperationLog
}
