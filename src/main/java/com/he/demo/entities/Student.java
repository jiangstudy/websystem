package com.he.demo.entities;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class Student {

    @ExcelProperty("name")  // ExcelProperty(index = 1)
    private String name;

    @ExcelProperty("age") // ExcelProperty(index = 2)
    private Integer age;

    @ExcelProperty("birthday")
    private Date birthday;

}
