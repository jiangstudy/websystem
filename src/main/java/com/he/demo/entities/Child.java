package com.he.demo.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RES1")
public class Child {

    private String name;

    private String isWrong;


    public String getName() {
        return name;
    }

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    public String getIsWrong() {
        return isWrong;
    }

    @XmlElement
    public void setIsWrong(String isWrong) {
        this.isWrong = isWrong;
    }
}
