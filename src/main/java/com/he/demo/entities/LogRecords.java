package com.he.demo.entities;

import java.util.Date;
import java.util.Map;

public class LogRecords {
    private Enum type;//类型
    private String level;//日志级别
    private Date operationTime;//操作时间
    private Map<String,Object> data;//日志内容

    public Enum getType() {
        return type;
    }

    public void setType(Enum type) {
        this.type = type;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Date getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(Date operationTime) {
        this.operationTime = operationTime;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
